# TrainingForGitlab

This is a repository used for training of gitlab.


# 1. Why do we learn GitLab?  


# 2. What is Gitlab?  
What is the relation between Gitlab and Git/GitHub?

# 3. Start with Git  
[Git Doc](https://git-scm.com/book/en/v2)

The git command you should remember: [Git command cheat sheet](https://github.github.com/training-kit/downloads/zh_CN/github-git-cheat-sheet/)

Git GUI tool suggested: [git cola](http://git-cola.github.io/)

# 4. Start with GitLab  
[Get started with GitLab](https://about.gitlab.com/get-started/)

[GitLab Markdown](https://gitlab.com/help/user/markdown#gitlab-flavored-markdown-gfm)

# 5. Start with GitHub  
[GitHub Hello World](https://guides.github.com/activities/hello-world/)

# 6. Start with BitBucket
[Learn Git](https://www.atlassian.com/git) 
[Get started with Bitbucket](https://confluence.atlassian.com/get-started-with-bitbucket/get-started-with-bitbucket-cloud-856845168.html) 

# 7. Learning resources
- [Learn Git in One Hour](https://www.cnblogs.com/best/p/7474442.html)
- [Using Git 1](https://github.github.com/training-kit/downloads/zh_CN/github-git-cheat-sheet/)
- [Using Git 2](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)
- [Resources to leran git](https://try.github.io/)
- [Learning git branch](https://learngitbranching.js.org/)
- [GitLab Doc](https://docs.gitlab.com/ee/user/index.html)
- [Training Materials](https://docs.gitlab.com/ee/university/training/)



## Assignment

1. Create your own Github/Gitlab/BitBucket Repository
2. Create several branches and push them into the repository
2. Use history command to record your command, like history > mylog.txt, and push the mylog.txt to the repository
